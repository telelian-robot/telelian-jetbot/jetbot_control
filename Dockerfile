FROM ros_humble_jetbot_build:latest


ARG PROJECT_NAME=jetbot_control
RUN mkdir -p /workspaces/src/$PROJECT_NAME
COPY . /workspaces/src/$PROJECT_NAME/
WORKDIR /workspaces/src

SHELL ["/bin/bash", "-c"] 

RUN pip install -r $PROJECT_NAME/requirements.txt \
    && source /opt/ros/humble/setup.bash \
    && cd /workspaces \
    && sudo rosdep install --from-paths src --ignore-src --rosdistro humble -y \
    && colcon build --symlink-install 

ENTRYPOINT [ "bash", "-c", "source install/setup.bash&& \"$@\"", "-s" ] 

