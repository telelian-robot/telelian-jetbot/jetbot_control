import math

import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from rclpy.clock import Time, Duration, ClockType
from rcl_interfaces.msg import SetParametersResult

from geometry_msgs.msg import Twist, Quaternion, TransformStamped
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32, Int32

from tf2_ros import TransformBroadcaster

from jetbot_control.jetbot_control import JetbotControl

# python logging level
# CRITICAL    50
# ERROR       40
# WARNING     30
# INFO        20
# DEBUG       10
# NOTSET      0

def quaternion_from_euler(roll, pitch, yaw):
    """
    Converts euler roll, pitch, yaw to quaternion (w in last place)
    quat = [x, y, z, w]
    Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
    """
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    q = [0] * 4
    q[0] = cy * cp * cr + sy * sp * sr
    q[1] = cy * cp * sr - sy * sp * cr
    q[2] = sy * cp * sr + cy * sp * cr
    q[3] = sy * cp * cr - cy * sp * sr

    return q

class Jetbot(Node):
    def __init__(self):
        super().__init__('jetbot')
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        
        self.declare_parameters(
            namespace='',
            parameters=[
                ('log-level', 'info'),
                ('port', "/dev/ttyACM0"),
                ('baudrate', 115200),
                ('max_speed', 1.0),
                ('linear_correction', 1.0),
                ('angular_correction', 0.5),
                ('kp', 350),
                ('ki', 120),
                ('kd', 0),
                
            ]
        )
        
        self._port = self.get_parameter('port').value
        self._baudrate = self.get_parameter('baudrate').value
        self._max_speed = self.get_parameter('max_speed').value
        self._log_level = self.get_logger().get_effective_level()
        
        self._max_speed = min(max(self._max_speed, -1.0), 1.0)
        
        self._linear_correction = self.get_parameter('linear_correction').value
        self._angular_correction = self.get_parameter('angular_correction').value
        
        self._kp = self.get_parameter('kp').value
        self._ki = self.get_parameter('ki').value
        self._kd = self.get_parameter('kd').value
        
        self.get_logger().info(f'port: {self._port}')
        self.get_logger().info(f'baudrate: {self._baudrate}')
        self.get_logger().info(f'max_speed: {self._max_speed}')
        self.get_logger().info(f'log-level: {self._log_level}')
        self.get_logger().info(f'linear_correction: {self._linear_correction}')
        self.get_logger().info(f'angular_correction: {self._angular_correction}')
        
        self.get_logger().info(f'kp: {self._kp}')
        self.get_logger().info(f'ki: {self._ki}')
        self.get_logger().info(f'kd: {self._kd}')
        
        
        self.add_on_set_parameters_callback(self.parameter_callback)
        
        self.cmdvel_sub = self.create_subscription(
            Twist,
            'cmd_vel',
            self.cmd_vel_callback,
            qos_profile
        )
        
        self.tf_broadcaster = TransformBroadcaster(self)        
        self.imu_pub = self.create_publisher(
            Imu,
            'imu',
            qos_profile
        )
        self.odom_pub = self.create_publisher(
            Odometry,
            'odom',
            qos_profile
        )
        self.lvel_pub = self.create_publisher(
            Int32,
            'motor/lvel',
            qos_profile
        )
        self.rvel_pub = self.create_publisher(
            Int32,
            'motor/rvel',
            qos_profile
        )
        self.lset_pub = self.create_publisher(
            Int32,
            'motor/lset',
            qos_profile
        )
        self.rset_pub = self.create_publisher(
            Int32,
            'motor/rset',
            qos_profile
        )
        
        self.prev_cmd_vel_time = None
        self.prev_time = None
        self._jetbot = JetbotControl(port=self._port,
                                     baudrate=self._baudrate, 
                                     debug_level=self._log_level, 
                                     topic_callback=self.topic_callback)
        self._jetbot.start()
        self._jetbot.set_params(self._linear_correction,self._angular_correction)
        
        
    def topic_callback(self, imu_list, odom_list, lvel, rvel, lset, rset):
        # self.get_logger().debug(f'topic_callback: {imu_list}, {odom_list}, {lvel}, {rvel}, {lset}, {rset}')
        
        now_time = self.get_clock().now()
        if self.prev_time is None:
            self.prev_time = now_time
        imu_msg = Imu()
        imu_msg.header.stamp = now_time.to_msg()
        imu_msg.header.frame_id = 'base_imu_link'
        imu_msg.angular_velocity.x = imu_list[0]
        imu_msg.angular_velocity.y = imu_list[1]
        imu_msg.angular_velocity.z = imu_list[2]
        imu_msg.linear_acceleration.x = imu_list[3]
        imu_msg.linear_acceleration.y = imu_list[4]
        imu_msg.linear_acceleration.z = imu_list[5]
        
        quat_orient = quaternion_from_euler(0,0,imu_list[8]/180*3.1415926)
        imu_msg.orientation = Quaternion(x=quat_orient[0], y=quat_orient[1], z=quat_orient[2], w=quat_orient[3])
        imu_msg.orientation_covariance = [1e6, 0.0, 0.0, 0.0, 1e6, 0.0, 0.0, 0.0, 0.05]
        imu_msg.angular_velocity_covariance = [1e6, 0.0, 0.0, 0.0, 1e6, 0.0, 0.0, 0.0, 1e6]
        imu_msg.linear_acceleration_covariance = [1e-2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]        
        self.imu_pub.publish(imu_msg)
        
        odom_trans = TransformStamped()
        odom_trans.header.stamp = now_time.to_msg()
        odom_trans.header.frame_id = 'odom'
        odom_trans.child_frame_id = 'base_footprint'
        
        odom_trans.transform.translation.x = odom_list[0]
        odom_trans.transform.translation.y = odom_list[1]
        odom_trans.transform.translation.z = 0.0
        quat_odom = quaternion_from_euler(0,0,odom_list[2])
        odom_trans.transform.rotation = Quaternion(x=quat_odom[0], y=quat_odom[1], z=quat_odom[2], w=quat_odom[3])
        self.tf_broadcaster.sendTransform(odom_trans)
        
        odom_msg = Odometry()
        odom_msg.header.stamp = now_time.to_msg()
        odom_msg.header.frame_id = 'odom'
        odom_msg.child_frame_id = 'base_footprint'
        
        odom_msg.pose.pose.position.x = odom_list[0]
        odom_msg.pose.pose.position.y = odom_list[1]
        odom_msg.pose.pose.position.z = 0.0
        odom_msg.pose.pose.orientation = Quaternion(x=quat_odom[0], y=quat_odom[1], z=quat_odom[2], w=quat_odom[3])
    
        duration = now_time - self.prev_time
        duration = duration.nanoseconds / 1e9
        if duration == 0:
            duration = 1
        odom_msg.twist.twist.linear.x = odom_list[3]/duration
        odom_msg.twist.twist.linear.y = odom_list[4]/duration
        odom_msg.twist.twist.angular.z = odom_list[5]/duration
        odom_msg.twist.covariance = [ 1e-9, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                    0.0, 1e-3, 1e-9, 0.0, 0.0, 0.0, 
                                    0.0, 0.0, 1e6, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 1e6, 0.0, 0.0, 
                                    0.0, 0.0, 0.0, 0.0, 1e6, 0.0, 
                                    0.0, 0.0, 0.0, 0.0, 0.0, 0.1 ]
        odom_msg.pose.covariance = [ 1e-9, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                        0.0, 1e-3, 1e-9, 0.0, 0.0, 0.0, 
                                        0.0, 0.0, 1e6, 0.0, 0.0, 0.0,
                                        0.0, 0.0, 0.0, 1e6, 0.0, 0.0, 
                                        0.0, 0.0, 0.0, 0.0, 1e6, 0.0, 
                                        0.0, 0.0, 0.0, 0.0, 0.0, 1e3 ]
        # publish the odom message
        self.odom_pub.publish(odom_msg)
        
        lvel_msg = Int32()
        lvel_msg.data = lvel
        rvel_msg = Int32()
        rvel_msg.data = rvel
        lset_msg = Int32()
        lset_msg.data = lset
        rset_msg = Int32()
        rset_msg.data = rset
        self.lvel_pub.publish(lvel_msg)
        self.rvel_pub.publish(rvel_msg)
        self.lset_pub.publish(lset_msg)
        self.rset_pub.publish(rset_msg)     
        
        self.prev_time = now_time
        
    def cmd_vel_callback(self, msg):
        # now_time = Time(nanoseconds=msg.header.stamp.nanoseconds, seconds=msg.header.stamp.seconds)
        now_time = self.get_clock().now()
        if self.prev_cmd_vel_time is None:
            self.prev_cmd_vel_time = now_time
        else:
            duration = now_time - self.prev_cmd_vel_time 
            duration_sec = duration.nanoseconds / 1e9
            if duration_sec < 0.02:
                self.get_logger().error(f'cmd_vel_callback too much: {duration_sec:.4f}s') 
                return
            else:
                self.get_logger().debug(f'cmd_vel_callback duration: {duration_sec:.4f}s')
        lx = -msg.linear.x
        ly = -msg.linear.y
        az = msg.angular.z
        lx = min(max(-self._max_speed, lx),self._max_speed)
        ly = min(max(-self._max_speed, ly),self._max_speed)
        az = min(max(-self._max_speed, az),self._max_speed)
        self.get_logger().debug(f'cmd_vel_callback limited: {lx},{ly}, {az}')
        self._jetbot.set_velocity(lx, ly, az)
        self.prev_cmd_vel_time = now_time
        
    def parameter_callback(self, params):
        
        for param in params:
            self.get_logger().info(f'parameter_callback: {param.name}, {param.type_}, {param.value}')
            if (param.name in ('linear_correction','angular_correction')
                and param.type_ == Parameter.Type.DOUBLE):
                setattr(self, f'_{param.name}', param.value)
                self._jetbot.set_params(self._linear_correction, self._angular_correction)
                return SetParametersResult(successful=True)
            elif param.name == 'log-level' and param.type_ in (Parameter.Type.INTEGER, Parameter.Type.STRING):
                self._log_level = param.value
                self._jetbot.set_debug_level(self._log_level)
                return SetParametersResult(successful=True)
            elif param.name in ('kp', 'kd', 'ki') and param.type_ == Parameter.Type.INTEGER:
                if param.value < 0 or param.value > 2000:
                    return SetParametersResult(successful=False
                    , reason=f'parameter range must be 0 to 2000. ({param.value}) cannot be set.')
                else:
                    setattr(self, f'_{param.name}', param.value)
                    self._jetbot.set_pid(self._kp, self._ki, self._kd)
                    return SetParametersResult(successful=True)
            
        return SetParametersResult(successful=False)
        
import sys
import argparse
import logging
def main(args=None):
    rclpy.init(args=args)
    node = Jetbot()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.get_logger().info('Keyboard Interrupt (SIGINT)')
        node._jetbot.close()
        node._jetbot.join()
        node.get_logger().debug('jetbot closed')
    finally:
        node.destroy_node()
    
if __name__ == '__main__':
    main()