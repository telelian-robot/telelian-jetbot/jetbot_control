import enum
import logging
import signal
import threading
import serial

HEAD1 = 0xAA
HEAD2 = 0x55
TYPE_VELOCITY = 0x11
TYPE_PID = 0x12
TYPE_PARAM = 0x13

CMD_SET_PARAM = 0x09
CMD_SET_VAL = 0x0b

def checksum(buf):
    sum = 0 
    for c in buf:
        sum += int(c)
    return sum & 0xff

def to_signed_int16(h, l):
    result = (h<<8) | l    
    return (result^0x8000) - 0x8000

class StateComm(enum.Enum):
    NONE = enum.auto()
    HEAD1 = enum.auto()
    HEAD2 = enum.auto()
    SIZE = enum.auto()
    DATA = enum.auto()
    CHECKSUM = enum.auto()
    HANDLE = enum.auto()

class JetbotControl(threading.Thread):
    def __init__(self, port='/dev/ttyACM0', baudrate=115200, debug_level=logging.INFO, topic_callback=None):
        super(JetbotControl, self).__init__()
        
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s' ,level=debug_level)
        self._quit = threading.Event()
        self._quit.clear()
        self._lock = threading.Lock()
        self._serial = serial.Serial(port=port,baudrate=baudrate, timeout=1)
        self._serial.flush()
        
        self.topic_callback = topic_callback
        
        self.prev_state = StateComm.NONE
        self.pkt = []
        self.frame_sum = 0
        self.frame_size = 1
        self.frame_type = 0
        self.frame_cnt = 0
    
    def set_debug_level(self, debug_level):
        try:
            logging.getLogger().setLevel(debug_level.upper())
            logging.info('set debug level to %s', debug_level)
        except ValueError:
            logging.error('invalid debug level %s', debug_level)
        
    def close(self):
        self._quit.set()
        self._quit.wait()
        self.join()
        
    def state_head1(self, ch):
        if ch == HEAD1:
            self.pkt = []
            self.frame_sum = 0
            self.frame_size = 1
            self.frame_type = 0
            self.frame_cnt = 0
            
            return StateComm.HEAD2
        return StateComm.HEAD1
    
    def state_head2(self, ch):
        if self.prev_state == StateComm.HEAD2 and ch == HEAD2:
            return StateComm.SIZE
        else:
            if ch == HEAD1:
                return StateComm.HEAD2
            logging.error(f'head2 error : {self.prev_state} 0x{ch:02x}') 
            return StateComm.HEAD1
            
    def state_size(self, ch):
        self.frame_size = ch
        return StateComm.DATA
    
    def state_data(self,ch):
        if self.frame_cnt == 0:
            self.frame_type = ch
            self.frame_cnt += 1
            
            return StateComm.DATA
        elif self.frame_cnt == self.frame_size-5:
            self.frame_cnt = 0            
            return StateComm.CHECKSUM
        else:
            self.frame_cnt += 1
            return StateComm.DATA
        
    def state_checksum(self,ch):
        chksum = checksum(self.pkt)
        
        if ch == chksum:
            self.handle(self.pkt)
        else:
            logging.error(f'chksum error : {ch} {chksum} {self.pkt}')
        
        return StateComm.HEAD1
    
    def handle(self,data):
        imu_list = [0] * 9
        # gyro
        imu_list[0] = to_signed_int16(data[4], data[5])/32768*2000/180*3.1415
        imu_list[1] = to_signed_int16(data[6], data[7])/32768*2000/180*3.1415
        imu_list[2] = to_signed_int16(data[8], data[9])/32768*2000/180*3.1415
        # Acc 
        imu_list[3] = to_signed_int16(data[10], data[11])/32768*2*9.8;
        imu_list[4] = to_signed_int16(data[12], data[13])/32768*2*9.8;
        imu_list[5] = to_signed_int16(data[14], data[15])/32768*2*9.8;
        # Angle 
        imu_list[6] = to_signed_int16(data[16], data[17])/10.0;
        imu_list[7] = to_signed_int16(data[18], data[19])/10.0;
        imu_list[8] = to_signed_int16(data[20], data[21])/10.0;
        
        # print(f'gyro: {imu_list[0]:03.3f} {imu_list[1]:03.3f} {imu_list[2]:03.3f}',end='\r')
        # print(f'acc: {imu_list[3]:03.3f} {imu_list[4]:03.3f} {imu_list[5]:03.3f}',end='\r')
        # print(f'angle: {imu_list[6]:03.3f} {imu_list[7]:03.3f} {imu_list[8]:03.3f}',end='\r')
        
        odom_list = [0] * 6
        odom_list[0] = to_signed_int16(data[22], data[23])/1000
        odom_list[1] = to_signed_int16(data[24], data[25])/1000
        odom_list[2] = to_signed_int16(data[26], data[27])/1000
        # dx dy dyaw base_frame
        odom_list[3] = to_signed_int16(data[28], data[29])/1000
        odom_list[4] = to_signed_int16(data[30], data[31])/1000
        odom_list[5] = to_signed_int16(data[32], data[33])/1000    
        
        lvel = to_signed_int16(data[34], data[35])
        rvel = to_signed_int16(data[36], data[37])
        lset = to_signed_int16(data[38], data[39])
        rset = to_signed_int16(data[40], data[41])
        
        if callable(self.topic_callback):
            self.topic_callback(imu_list, odom_list, lvel, rvel, lset, rset)
        
        
    def run(self):
        logging.debug(f'{self.__class__.__name__} started')
        state = StateComm.HEAD1
        self.prev_state = state
        
        while not self._quit.is_set():
            try:
                buf = self._serial.read(1)
                if len(buf)>0:
                    buf = int.from_bytes(buf, 'little')
                    
                    state = {
                        StateComm.HEAD1: self.state_head1,
                        StateComm.HEAD2: self.state_head2,
                        StateComm.SIZE: self.state_size,
                        StateComm.DATA: self.state_data,
                        StateComm.CHECKSUM : self.state_checksum,
                    }.get(state)(buf)
                    self.pkt.append(buf)
                    self.prev_state = state
                    
                
            except serial.SerialException as e:
                logging.error(e)
        logging.debug('close thread')
                    
    def _registry_signal(self):
        def handler(signum, frame):
            logging.critical(
                {
                    "title": "signal",
                    "signum": signum,
                    "frame": frame,
                }
            )
            self.close()

        for signame in {signal.SIGINT, signal.SIGTERM}:
            signal.signal(signame, handler)

   
    def set_params(self, linear_correction, angular_correction):
        lc = int(linear_correction * 1000)
        ac = int(angular_correction * 1000)
        buf =[
            HEAD1,
            HEAD2,
            CMD_SET_PARAM,
            TYPE_PARAM, 
            (lc >>8)&0xff,
            lc&0xff,
            (ac >>8)&0xff,
            ac&0xff,            
        ]
        buf.append(checksum(buf))
        logging.debug(f'param: {buf}')
        self._serial.write(bytes(buf))
    
    def set_velocity(self, x, y, yaw):
        x = int(x*1000)
        y = int(y*1000)
        yaw = int(yaw*1000)
        
        buf = [
            HEAD1,
            HEAD2,
            CMD_SET_VAL,
            TYPE_VELOCITY, 
            (x >>8)&0xff,
            x&0xff,
            (y >>8)&0xff,
            y&0xff,
            (yaw >>8)&0xff,
            yaw&0xff,    
        ]
        buf.append(checksum(buf))
        logging.debug(f'velocity: {buf}')
        self._serial.write(bytes(buf))
    
    def set_pid(self, p, i, d):        
        buf = [
            HEAD1,
            HEAD2,
            CMD_SET_VAL,
            TYPE_PID, 
            (p >>8)&0xff,
            p&0xff,
            (i >>8)&0xff,
            i&0xff,
            (d >>8)&0xff,
            d&0xff,    
        ]
        buf.append(checksum(buf))
        logging.debug(f'pid: {buf}')
        self._serial.write(bytes(buf))    
    
def main():
    odom = JetbotControl(debug_level=logging.DEBUG)
    odom._registry_signal()
    odom.set_params(1,1)
    odom.set_velocity(-0.1,0,0)
    odom.start()
    odom.join()
    
if __name__ == '__main__':
    main()