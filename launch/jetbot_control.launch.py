from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, TextSubstitution
from launch_ros.actions import Node

def generate_launch_description():
    jetbot_control_dir = get_package_share_directory('jetbot_control')
    config_path = f'{jetbot_control_dir}/config/config.yaml'
    namespace = LaunchConfiguration('namespace')
    
    return LaunchDescription([
        DeclareLaunchArgument(
            "namespace",
            default_value='jetbot_ros_ai',
        ),
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'
        ),
        DeclareLaunchArgument(
            "log-level",
            default_value = TextSubstitution(text=str("INFO")),
            description="Logging level"
        ),
        Node(
            package='jetbot_control',
            executable='jetbot_node',
            name='jetbot',
            namespace=namespace,
            parameters=[config_path],
            arguments=['--ros-args', '--log-level', LaunchConfiguration('log-level')],
            output='screen'
        ),
    ])